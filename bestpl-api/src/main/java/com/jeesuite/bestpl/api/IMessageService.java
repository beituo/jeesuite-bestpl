package com.jeesuite.bestpl.api;

import java.util.List;

import com.jeesuite.bestpl.dto.Message;

public interface IMessageService {

	List<Message> findUserNotReadMessage(int userId);
	
	void addMessage(Message message);
}
