package com.jeesuite.bestpl.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jeesuite.common.util.DateUtils;

public class Post implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	/**
	 * 语言id
	 */
	private Integer userId;

	/**
	 * 语言id
	 */
	private String userName;

	/**
	 * 语言名称
	 */
	private String title;

	/**
	 * 栏目id
	 */
	private Integer categoryId;

	/**
	 * 评论数
	 */
	private Integer commentCount;

	/**
	 * 评论数
	 */
	private Integer viewCount;

	/**
	 * 是否推荐
	 */
	private Boolean isRecommend;

	/**
	 * 是否原创
	 */
	private Boolean isOriginal;

	/**
	 * 创建时间
	 */
	private Date createdAt;

	private String seoKeywords;

	private String seoDesc;

	/**
	 * 摘要
	 */
	private String summary;

	/**
	 * 内容
	 */
	private String content;

	private String category;

	@JsonIgnore
	private String tags;

	private List<String> tagList;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public Integer getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(Integer commentCount) {
		this.commentCount = commentCount;
	}

	public Integer getViewCount() {
		return viewCount;
	}

	public void setViewCount(Integer viewCount) {
		this.viewCount = viewCount;
	}

	public Boolean getIsRecommend() {
		return isRecommend;
	}

	public void setIsRecommend(Boolean isRecommend) {
		this.isRecommend = isRecommend;
	}

	public Boolean getIsOriginal() {
		return isOriginal;
	}

	public void setIsOriginal(Boolean isOriginal) {
		this.isOriginal = isOriginal;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getSeoKeywords() {
		return seoKeywords;
	}

	public void setSeoKeywords(String seoKeywords) {
		this.seoKeywords = seoKeywords;
	}

	public String getSeoDesc() {
		return seoDesc;
	}

	public void setSeoDesc(String seoDesc) {
		this.seoDesc = seoDesc;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
    
	
	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public List<String> getTagList() {
		if (tagList == null && StringUtils.isNotBlank(tags)) {
			tagList = new ArrayList<>(Arrays.asList(tags.split(",|;")));
		}
		return tagList;
	}

	public void setTagList(List<String> tagList) {
		this.tagList = tagList;
	}

	public String getFormatCreateTime(){
		if(getCreatedAt() == null)return null;
		long diffSeconds = DateUtils.getDiffSeconds(new Date(), getCreatedAt());
		if(diffSeconds >= 2592000){
			return (diffSeconds/2592000) + " 月前";
		}
		if(diffSeconds >= 86400){
			return (diffSeconds/86400) + " 天前";
		}
		if(diffSeconds >= 3600){
			return (diffSeconds/3600) + " 小时前";
		}
		if(diffSeconds >= 60){
			return (diffSeconds/60) + " 分钟前";
		}
		
		return diffSeconds + " 秒前";
		
	}

}
