package com.jeesuite.bestpl.dto;

import com.jeesuite.common.util.ResourceUtils;
import com.jeesuite.passport.LoginContext;
import com.jeesuite.passport.model.LoginUserInfo;

public class PageMetaInfo {
	
	public static final String ATTR_NAME = "info";

	private String title = "bestpl社区";
	private String subTitle = "jeesuite演示项目";
	private String keywords = "jeesuite";
	private String description = "jeesuite";
	private String modelName;
	private String staticBasePath;
	
	private LoginUserInfo loginUser;
	
	public static PageMetaInfo current(){
		PageMetaInfo pageMetaInfo = new PageMetaInfo();
		pageMetaInfo.loginUser = LoginContext.getLoginUserInfo();
		pageMetaInfo.staticBasePath = ResourceUtils.getProperty("static.base.url");
		return pageMetaInfo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public PageMetaInfo setSubTitle(String subTitle) {
		this.subTitle = subTitle;
		return this;
	}

	public String getKeywords() {
		return keywords;
	}

	public PageMetaInfo setKeywords(String keywords) {
		this.keywords = keywords;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public PageMetaInfo setDescription(String description) {
		this.description = description;
		return this;
	}

	public String getModelName() {
		return modelName;
	}

	public PageMetaInfo setModelName(String modelName) {
		this.modelName = modelName;
		return this;
	}

	public LoginUserInfo getLoginUser() {
		return loginUser;
	}

	public String getStaticBasePath() {
		return staticBasePath;
	}

	public void setStaticBasePath(String staticBasePath) {
		this.staticBasePath = staticBasePath;
	}
    	
    
}
