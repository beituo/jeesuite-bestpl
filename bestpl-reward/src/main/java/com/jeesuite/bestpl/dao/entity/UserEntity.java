package com.jeesuite.bestpl.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import com.jeesuite.mybatis.core.BaseEntity;

@Table(name = "sc_user")
public class UserEntity extends BaseEntity {
	
	public static enum UserStatus{
		d,n,a
	}
	
    /**
     * 主键ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 登录名
     */
    @Column(name = "username",updatable=false)
    private String username;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 实名
     */
    private String realname;

    /**
     * 密码
     */
    private String password;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 邮件
     */
    private String email;

    /**
     * 邮箱状态（是否认证等）
     */
    @Column(name = "email_verifyed")
    private Boolean emailVerifyed;

    /**
     * 手机电话
     */
    private String mobile;

    /**
     * 手机状态（是否认证等）
     */
    @Column(name = "mobile_verifyed")
    private Boolean mobileVerifyed;

    /**
     * 性别
     */
    private String gender;

    /**
     * 用户类型(1:普通用户,2:内部编辑)
     */
    @Column(name = "user_type",updatable=false)
    private Integer userType;

    /**
     * 签名
     */
    private String signature;

    /**
     * 内容数量
     */
    @Column(name = "post_count")
    private Integer postCount;

    /**
     * 评论数量
     */
    @Column(name = "comment_count")
    private Integer commentCount;

    /**
     * 声望值
     */
    private Integer points;

    /**
     * 状态(d:删除，n:未激活：,a:激活)
     */
    private String status;

    /**
     * 创建日期
     */
    @Column(name = "created_at",updatable=false)
    private Date createdAt;

    /**
     * 最后的登录时间
     */
    @Column(name = "logged_at")
    private Date loggedAt;

    /**
     * 获取主键ID
     *
     * @return id - 主键ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置主键ID
     *
     * @param id 主键ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取登录名
     *
     * @return username - 登录名
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置登录名
     *
     * @param username 登录名
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 获取昵称
     *
     * @return nickname - 昵称
     */
    public String getNickname() {
        return StringUtils.isBlank(nickname) ? username : nickname;
    }

    /**
     * 设置昵称
     *
     * @param nickname 昵称
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * 获取实名
     *
     * @return realname - 实名
     */
    public String getRealname() {
        return realname;
    }

    /**
     * 设置实名
     *
     * @param realname 实名
     */
    public void setRealname(String realname) {
        this.realname = realname;
    }

    /**
     * 获取密码
     *
     * @return password - 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置密码
     *
     * @param password 密码
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 获取头像
     *
     * @return avatar - 头像
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * 设置头像
     *
     * @param avatar 头像
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * 获取邮件
     *
     * @return email - 邮件
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置邮件
     *
     * @param email 邮件
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取邮箱状态（是否认证等）
     *
     * @return email_verifyed - 邮箱状态（是否认证等）
     */
    public Boolean getEmailVerifyed() {
        return emailVerifyed;
    }

    /**
     * 设置邮箱状态（是否认证等）
     *
     * @param emailVerifyed 邮箱状态（是否认证等）
     */
    public void setEmailVerifyed(Boolean emailVerifyed) {
        this.emailVerifyed = emailVerifyed;
    }

    /**
     * 获取手机电话
     *
     * @return mobile - 手机电话
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机电话
     *
     * @param mobile 手机电话
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取手机状态（是否认证等）
     *
     * @return mobile_verifyed - 手机状态（是否认证等）
     */
    public Boolean getMobileVerifyed() {
        return mobileVerifyed;
    }

    /**
     * 设置手机状态（是否认证等）
     *
     * @param mobileVerifyed 手机状态（是否认证等）
     */
    public void setMobileVerifyed(Boolean mobileVerifyed) {
        this.mobileVerifyed = mobileVerifyed;
    }

    /**
     * 获取性别
     *
     * @return gender - 性别
     */
    public String getGender() {
        return gender;
    }

    /**
     * 设置性别
     *
     * @param gender 性别
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * 获取用户类型(1:普通用户,2:内部编辑)
     *
     * @return user_type - 用户类型(1:普通用户,2:内部编辑)
     */
    public Integer getUserType() {
        return userType;
    }

    /**
     * 设置用户类型(1:普通用户,2:内部编辑)
     *
     * @param userType 用户类型(1:普通用户,2:内部编辑)
     */
    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    /**
     * 获取签名
     *
     * @return signature - 签名
     */
    public String getSignature() {
        return signature;
    }

    /**
     * 设置签名
     *
     * @param signature 签名
     */
    public void setSignature(String signature) {
        this.signature = signature;
    }

    /**
     * 获取内容数量
     *
     * @return post_count - 内容数量
     */
    public Integer getPostCount() {
        return postCount;
    }

    /**
     * 设置内容数量
     *
     * @param postCount 内容数量
     */
    public void setPostCount(Integer postCount) {
        this.postCount = postCount;
    }

    /**
     * 获取评论数量
     *
     * @return comment_count - 评论数量
     */
    public Integer getCommentCount() {
        return commentCount;
    }

    /**
     * 设置评论数量
     *
     * @param commentCount 评论数量
     */
    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    /**
     * 获取声望值
     *
     * @return points - 声望值
     */
    public Integer getPoints() {
        return points;
    }

    /**
     * 设置声望值
     *
     * @param points 声望值
     */
    public void setPoints(Integer points) {
        this.points = points;
    }

    /**
     * 获取状态(d:删除，n:未激活：,a:激活)
     *
     * @return status - 状态(d:删除，n:未激活：,a:激活)
     */
    public String getStatus() {
        return status;
    }

    /**
     * 设置状态(d:删除，n:未激活：,a:激活)
     *
     * @param status 状态(d:删除，n:未激活：,a:激活)
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 获取创建日期
     *
     * @return created_at - 创建日期
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * 设置创建日期
     *
     * @param createdAt 创建日期
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 获取最后的登录时间
     *
     * @return logged_at - 最后的登录时间
     */
    public Date getLoggedAt() {
        return loggedAt;
    }

    /**
     * 设置最后的登录时间
     *
     * @param loggedAt 最后的登录时间
     */
    public void setLoggedAt(Date loggedAt) {
        this.loggedAt = loggedAt;
    }
}